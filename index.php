<!--============= HEADER =============-->
<?php include("header.php"); ?>
<!--============= SECTION I =============-->
<div class="section1">
  <div class="container position">
    <div class="sec1-lft">
      <p class="s1-hd2">Popular Online Superstore For<span>Gears & Gadgets</span></p>
      <p class="clearall"></p>
      <p class="s1-rgt-txt">Uber-cool accessories designed using next level technology in tune with the demands of the new generation.</p>
      <p class="clearall"></p>
      <a href="products.php" class="s1-ord-btn">Place Order!</a> </div>
  </div>
</div>
<!--============= SECTION I BOTTOM STRIP =============-->
<div class="section2">
  <div class="container">
    <p class="s1-btm-p3"><span>Stay Smart With</span> Bright Supplies US</p>
    <p class="s1-btm-p4">Reading complicated user manuals or dealing with technical hiccups need investing extra effort, which the modern generation can barely manage to spare.<br>
      <br>
      <b>Bright Supplies US</b> understands the woes of modern lifestyle and presents an exclusive list of high-quality gears & gadgets enhanced with the latest technology. User-friendly features support convenient operation, efficient performance saves time & energy and greater life span assure viability. </p>
    <p class="clearall"></p>
    <ul class="s1btm-list1">
      <li> <img src="images/s2-icn1.png" alt="" class="s2-icn1"> Quality products </li>
      <li> <img src="images/s2-icn2.png" alt="" class="s2-icn1">Safe payment gateway </li>
      <li> <img src="images/s2-icn3.png" alt="" class="s2-icn1">Pocket-friendly </li>
      <li> <img src="images/s2-icn4.png" alt="" class="s2-icn1"> Customer support </li>
    </ul>
  </div>
</div>
<!--============= SECTION II =============--> 

<!--============= SECTION III =============--> 
<!--============= BOTTOM STRIP =============-->
<div class="section-4">
  <div class=" container">
    <div class="sec4-lft">
      <p class="s1-btm-p3"><span>Get Your </span>Products Today!</p>
      <p class="s1-btm-p4">Take a look at our bestselling product range and make an informed choice.</p>
      <a href="products.php" class="s1-ord-btn">Place Order!</a> </div>
      <img src="images/s4-prod.png" class="s4-prod">
  </div>
</div>
</div>
<div class="section3">
  <div class="container" id="001">
    <p class="s1-btm-p3"><span>EXPLORE OUR</span>Limited Stock List</p>
    <div class="clearall"></div>
    <div class="s3-block position">
      <div class="s3-box2">
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][5]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt"> <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][5]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][5]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('5');?>" class="s1-ord-btn"> Place Order!</a> </div>
      </div>
      <div class="s3-box2">
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][4]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt"> <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][4]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][4]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('4');?>" class="s1-ord-btn">Place Order! </a> </div>
      </div>
      <div class="s3-box2">
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][2]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt"> <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][2]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][2]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('2');?>" class="s1-ord-btn"> Place Order! </a> </div>
      </div>
    </div>
    <p class="clearall"></p>
    <a href="products.php" class="s1-ord-btn">Explore All</a> </div>
</div>
<!--============= FOOTER =============-->

<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script> 
<script src="js/bookmarkscroll.js"></script> 
<script type="text/javascript" src="js/slick.js"></script> 
<script type="text/javascript">
$(document).ready(function(){	
	$('.banner').slick({
		dots: true,			
		autoplay: false,			
		adaptiveHeight: true,
		fade: false,
		focusOnSelect: true,
		arrows: false,
		slidesToScroll: 1,
		slidesToShow: 1,
	});	
});
</script>
</body>
</html>
