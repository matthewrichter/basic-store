<?php 
include "../config.inc.php";
?>
var products = [];
<?php foreach($config['productSpecs'] as $prodIndex => $prodSpec) { ?>
products[<?php echo $prodSpec['id']; ?>] = ["<?php echo utf8_encode($prodSpec['nm']); ?>", <?php echo utf8_encode($prodSpec['prc']); ?>, "<?php echo utf8_encode($prodSpec['altnm']); ?>", "<?php echo utf8_encode($prodSpec['cartimg']); ?>","<?php echo utf8_encode($prodSpec['desc']); ?>", "<?php echo utf8_encode($prodSpec['chkterms']); ?>", <?php echo utf8_encode($prodSpec['shp-prc']); ?>];
<?php } ?>
console.log(products);
var orderedItems = [];
var orderedTotQty = 0;
var maxQty = <?php echo $config['max_qty']; ?>;
var shipRates = false;
var handling = 0;

var carturl = "<?php echo $config['cart_page'] ; ?>";

//Funtion adds Items to Cart
var addItem = function(itemId, Qty) {
	orderedTotQty = $.cookie('orderedTotQty');
	if (!orderedTotQty) {
		orderedTotQty = 0;
	}
    Qty = parseInt(Qty);
	if(Qty > maxQty) {
    	alert("Maximum " + maxQty + " of this product can be selected in a single order");
		return false;
	}
    var order = $.cookie('order');
    if (!order) {
        order = itemId + "-" + Qty;
		orderedItems[itemId] = Qty;
    } else {
        var itemExists = false;
        var items = order.split("|");
        order = "";
        for (var i = 0; i < items.length; i = i + 1) {
            var position = items[i].indexOf("-");
            var prodId = items[i].substring(0, position);
            var quantity = parseInt(items[i].substring(position + 1));
            if (order != "") {
                order += "|";
            }
            if (prodId == itemId) {
            	alert("Product already exist in cart");
                return false;
				if((quantity + Qty) > maxQty) {
                    alert("Maximum " + maxQty + " of this product can be selected in a single order");
					return false;
				}
                quantity += Qty;
                order += prodId + "-" + quantity;
                itemExists = true;
            } else {
                order += prodId + "-" + quantity;
            }
			
			orderedItems[prodId] = quantity;
        }
        if (!itemExists) {
            if (order != "") {
                order += "|";
            }
            order += itemId + "-" + Qty;
			orderedItems[itemId] = Qty;
        }
    }
	orderedTotQty = parseInt(orderedTotQty);
	orderedTotQty += Qty;
	$('.cartqty').html(orderedTotQty);
    $.cookie('order', order);
    $.cookie('orderedTotQty', orderedTotQty);

    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf('/') + 1);
    if (filename == carturl) {
        showCart(false, 'add', itemId);
    } else {
        $(location).attr('href', carturl);
    }
};

//Funtion decrements Items to Cart
var removeItem = function(itemId, Qty) {
    Qty = parseInt(Qty);
    var order = $.cookie('order');

    if (!order) {
		orderedItems[itemId] = 0;
		refreshRow(itemId);
		
    } else {
        var items = order.split("|");
        order = "";
        for (var i = 0; i < items.length; i = i + 1) {
            var position = items[i].indexOf("-");
            var prodId = items[i].substring(0, position);
            var quantity = parseInt(items[i].substring(position + 1));
            if (prodId == itemId) {
                quantity -= Qty;
                if (quantity > 0) {
					if (order != "") {
						order += "|";
					}
                    order += prodId + "-" + quantity;
                }
                itemExists = true;
				orderedItems[prodId] = quantity;
				refreshRow(itemId);
            } else {
				if (order != "") {
					order += "|";
				}
                order += prodId + "-" + quantity;
				orderedItems[prodId] = quantity;
            }
        }
    }
	orderedTotQty -= Qty;
	$('.cartqty').html(orderedTotQty);
    $.cookie('order', order);
    $.cookie('orderedTotQty', orderedTotQty);
    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf('/') + 1);
    if (filename == carturl) {
       showCart(false, 'remove', itemId);
    } else {
        $(location).attr('href', carturl);
    }
};


//Funtion sets Item quantity on the Cart
var setItemQty = function(itemId, Qty) {
    Qty = parseInt(Qty);
	
	if(Qty > maxQty || Qty < 0) {
		return false;
	}
	
    var order = $.cookie('order');
	orderedTotQty = 0;

    if (!order) {
		orderedItems[itemId] = 0;
    } else {
        var items = order.split("|");
        order = "";
        for (var i = 0; i < items.length; i = i + 1) {
            var position = items[i].indexOf("-");
            var prodId = items[i].substring(0, position);
            var quantity = parseInt(items[i].substring(position + 1));
            if (prodId == itemId) {
                quantity = Qty;
				if (order != "") {
					order += "|";
				}
				order += prodId + "-" + quantity;
                itemExists = true;
            } else {
				if (order != "") {
					order += "|";
				}
                order += prodId + "-" + quantity;
            }
			orderedItems[prodId] = quantity;
			orderedTotQty += quantity;
        }
    }
	$('.cartqty').html(orderedTotQty);
    $.cookie('order', order);
    $.cookie('orderedTotQty', orderedTotQty);
    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf('/') + 1);
    if (filename == carturl) {
       showCart(false, 'set', itemId);
    } else {
        $(location).attr('href', carturl);
    }
};



var removeRowItem = function(itemId) { 
    var order = $.cookie('order');
    if (!order)
    {
		orderedTotQty = 0;
    } else {
        var items = order.split("|");
        order = "";
		orderedTotQty = 0;
		orderedItems = null;
		orderedItems = new Array();
        for (var i = 0; i < items.length; i = i + 1) {
            var position = items[i].indexOf("-");
            var prodId = items[i].substring(0, position);
            var quantity = parseInt(items[i].substring(position + 1));
            if (prodId == itemId) {
            } else {
				if (order != "") {
					order += "|";
				}
                order += prodId + "-" + quantity;
				orderedTotQty += quantity;
				orderedItems[prodId] = quantity;
            }
        }
    }
	if($('#prow-' + itemId).size() == 1) {
		$('#prow-' + itemId).remove();
        $('#term-' + itemId).remove();
	}
    $.cookie('order', order);
    $.cookie('orderedTotQty', orderedTotQty);
    showCart(false, 'removerow', itemId);
};

//Emptying the cart
var emptyCart = function() {
    var order = $.cookie('order');
    order = "";
	orderedTotQty = 0;
	orderedItems = null;
	orderedItems = new Array();	
    $.cookie('order', order);
    $.cookie('orderedTotQty', orderedTotQty);
	
	if($('[id^="prow-"]').size() > 0) {
		$('[id^="prow-"]').each(function(){
			$(this).remove();
		});
	}
	$('.cart-terms span').hide();
    showCart(false, 'empty');
};

//Displaying the cart items & calculations
function showTotalPrice() {	
	var cartHtml = "";
	var total = 0;
	var shipping = 0;
	var grand_total = 0;
	var sub_total = 0;
	var shippingType = '';
	
	var order = $.cookie('order');
    orderedTotQty = $.cookie('orderedTotQty');
	
	
	if($('#shippingCountry').val() == '') {
		shippingType = '';
	} else if($('#shippingCountry').val() == 'US') {
		shippingType = 'US';
	} else {
		shippingType = 'INTERNATIONAL';
	}
	
    if (!order) {
		orderedTotQty = 0;
	} else {
		var items = order.split("|");

		if(shipRates) {
			//shipping = shipRates * 1.0;
		}
        shipping = 0;

		for (var i = 0; i < items.length; i = i + 1) {
			var position = items[i].indexOf("-");
			var prodId = items[i].substring(0, position);
			var quantity = parseInt(items[i].substring(position + 1));
			if (prodId != "" && quantity > 0) {
                sub_total += round((quantity * products[prodId][1]), 2);
                shipping +=  products[prodId][6];
                
			}
		}
        
        
	}
	
	grand_total = sub_total + shipping;
	
	$('#total-price').html('$' + sub_total.toFixed(2));
	$('#shipping-total').html('$' + shipping.toFixed(2));
	$('#grand-total').html('$' + grand_total.toFixed(2));

    if($(document).find("input[name='promoCode']").filter(':visible:first').val() != '' && $('.discountBtn').val() == 'APPLIED') {
   		applyCoupon($(document).find("input[name='promoCode']").filter(':visible:first').val());
   }
};

// Refresh row content with updated quantity / price for a product
function refreshRow(pid) {
	pid = parseInt(pid);
	quantity = orderedItems[pid];
	sub_total = round((quantity * products[pid][1]), 2);

	$('#prow-' + pid + ' .tot-price').html('$' + sub_total.toFixed(2) + ' USD');
	$('#prow-' + pid + ' .qtybox').val(quantity);
	$('#prow-' + pid + ' .dispqty').html(quantity);
	
}

//Displaying the cart items & calculations
function showCart(showFullCart, act, itm) {	
    var cartHtml = "";
    var order = $.cookie('order');
    orderedTotQty = $.cookie('orderedTotQty');
	
    if (!order)
    {
		orderedTotQty = 0;

		if($('[id^="prow-"]').size() == 0) {
	        $("#cartBox").html("You have not selected any product... &nbsp <a id='browse' href='products.php'>Browse Products</a>");
		}

		showTotalPrice();
		return false;
    }
    else
    {
        var items = order.split("|");
        var row_total = 0;
        var shipping = 0;
        var sub_total = 0;
        var total = 0;
        var grand_total = 0;
		
		orderedTotQty = parseInt(orderedTotQty);
		
		if (typeof showFullCart === "undefined") {
			return false;
		} else if(showFullCart == false) {
			if ((typeof act !== "undefined") && (typeof itm !== "undefined")) {
				if((act == 'add' || act == 'set' || act == 'remove') && itm > 0) {
					refreshRow(itm);
				} else if(act == 'removerow' && itm > 0) {
					itm = parseInt(itm);
				}
			}

			showTotalPrice();
			
			return false;
		} 
		
		orderedItems = null;
		orderedItems = new Array();
		<!--cartHtml += "<div class='summry-lft'><div class='summry-lft-hdr'>Order Summary <span>Price</span></div>";-->
        cartHtml += "<div class='cart_hdr'><span class='prd_name'><img src='images/shop-bag.png'>Product</span><span class='prd_prc'>Price</span><span class='prd_qty'>Qty</span><span class='prd_ship'>S&H </span><span class='prd_total'>Total</span></div>";
		var total = 0;
        var term_htm ='';
        for (var i = 0; i < items.length; i = i + 1) {
            var position = items[i].indexOf("-");
            var prodId = items[i].substring(0, position);
            var quantity = parseInt(items[i].substring(position + 1));
			
            if (prodId != "" && quantity > 0) {
				orderedItems[prodId] = quantity;
                //var sub_total = round((quantity * products[prodId][1]), 2);
                
                row_total =  round((quantity * products[prodId][1]), 2) + products[prodId][6];
                sub_total += round((quantity * products[prodId][1]), 2);
                shipping +=  products[prodId][6];
                
                term_htm += '<span id="term-' + prodId +'">' + products[prodId][5] + '</span>';
                 cartHtml += "<div class='prod_details' id='prow-" + prodId + "'>"
                			+ "<img style='cursor:pointer' src='images/cross-cart.png' class='cart-remv' onClick='removeRowItem(" + prodId + ")' />"
                			+ "<div class='prod_name'>" + products[prodId][3] + "<span class='product_name'>" +  products[prodId][0] + "<span>" +  products[prodId][2] + "</span></span></div>"
                			+ "<input type='hidden' value='" + quantity + "' name='prodqty_" + prodId + "' class='qtybox' readonly>"
							+ "<input type='hidden' value='" + prodId + "' name='prodid[]'>"                            
                            + "<div class='cart_prod_prc'> $" + (products[prodId][1]).toFixed(2) + "  </div>"
                            + "<div class='cart_prod_qty'>" + quantity + "</div>"
                            + "<div class='cart_prod_shp'>$" + products[prodId][6].toFixed(2) + "</div>"
                            + "<div class='cart_prc_subtotl'>$" + row_total.toFixed(2) + "</div>"
           					+ "</div>"
            }
        }
        grand_total = sub_total + shipping;
        
        cartHtml += "<!--</div>-->";
        cartHtml += "<div class='cart-totl'>"
                		+"<div classs='cart-summery'>"
                        +"<div class='cart-sb-totl'>"
                    		+"<span class='sub-lbl'>Sub Total :</span>"
                    		+"<span class='sub-prc' id='total-price'>$ " + sub_total.toFixed(2) + "</span>"
                		+"</div>"
                        
                        + "<div class='promoBox'>"
                            + "<input type='text' name='promoCode' id='promoCode' class='promoFld' placeholder='Enter Promo Code'>"
                            + "<input type='button' class='promoBtn discountBtn' value='Apply'>"
                        + "</div>"
                        +"<div class='dis-charge' style='display:none'>"
                            +"<span class='shp-lbl'>Discount (50%):</span>"
                            +"<span class='shp-prc' id='disprc'></span>"
                        +"</div>"
                        +"<div class='shp-charge'>"
                            +"<span class='shp-lbl'>Shipping & Handling :</span>"
                            +"<span class='shp-prc' id='shipping-total'>$" + shipping.toFixed(2) + "</span>"
                        +"</div>"
                        +"<div class='total-amt'>"
                            +"<span class='total-lbl'>Total :</span>"
                            +"<span class='total-prc' id='grand-total'>$ " + grand_total.toFixed(2) + "</span>"
                        +"</div><div class='cart-links'><a href='javascript:void(0)' onclick='emptyCart()' class='prod-clearcart'>Empty cart</a><a href='products.php' class='cont-shop'>Continue Shopping</a></div></div>"
            		+"</div>";
                    
        if (cartHtml != "") {
            $("#cartBox").html(cartHtml);
            $(".trm_1").html(term_htm);
        } else {
            $("#cartBox").html("&nbsp;Loading...");
        }
		
		return false;
    }
};

var round = function(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
};


