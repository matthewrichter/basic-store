<!--============= HEADER =============-->

<?php include("header.php");?>
<link href="css/cart.css" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="css/validationEngine.jquery.css" />

<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.h5validate.js"></script>
<script type="text/javascript" src="js/cartv5.js.php"></script>
<script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
<script src="js/common.js"></script>
<style type="text/css">
.formError {
	position: relative;
	left: auto;
	top: auto;
}
input:invalid {
/*background-color:#fff;*/
}
#frm-box label {
/*font-weight:normal;*/
}
input[type='text'], input[type='number'], input[type='email'] {
/*padding-left:3px;
	padding-right:3px;*/
}
input[type='text'].formError, input[type='number'].formError, input[type='email'].formError, select.formError {
	border: 1px solid orange !important;
	background-image: url("images/invalid.png")!important;
	background-repeat: no-repeat!important;
	background-position: 97% center;
	position: relative;
	display: inline-block;
	top: auto;
	left: auto;
}
label.formError {
	border: 1px solid orange !important;
	background-image: url("images/invalid.png")!important;
	background-repeat: no-repeat!important;
	background-position: 2px 2px;
}
select.formError {
/*background-position: 8% center;*/
}
div.formError {
	border: 1px solid orange !important;
	background-image: url("images/invalid.png")!important;
	background-repeat: no-repeat!important;
	background-position: 13px 10px;
}
</style>



<!--============= COMMON HEADER =============-->

<div class="commen-banner">
    <div class="container">
    <ul class="brdcmb">
          <li><a href="index.php">Home</a></li>
          <li> Cart</li>
        </ul>
    	 <p class="common-head">Your Shopping Cart</p>
    </div>
</div>

<div id="processing-box-overlay"></div>
<div id="processing-box-inner" style="display:none;">
	<img src="images/loading.gif" /> Checking Availability...
</div>
  
<!--============= CART SECTION =============-->

<p class="clearall"></p>
<div id="cart-box">
    <div class="prd-details">
        <div class="container" style=" display:table;">
        <div class="clearall"></div>
            <div id="contents">
                <div class="product-page" id="divOrder">
					<?php include('_cart_form.php'); ?>
                    <div class="clear-fix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--============= FOOTER =============-->

<?php include 'footer.php'; ?>

<script type='text/javascript'>

var countries = ['US'];
$(document).ready(function() {
		
    showCart(true);
	
	var selShipCountry = 'US';
    if (selShipCountry && selShipCountry != '') {
		getStates('#frmOrder', 'shipping', selShipCountry, '', 'billing', '' );
	}
	$('select[name=shippingCountry]', '#frmOrder').change(function(){
		getStates('#frmOrder', 'shipping', '', '', 'billing', '');
	});
	
	$.h5Validate.addPatterns({
		phone: /^(\d{3}[\s.-]?\d{3}[\s.-]?\d{4})|(\d{10})$/ ,
		zip: /(^\d{5}$)|(^\d{4}$)|(^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]\d[ABCEGHJKLMNPRSTVWXYZ]\d$)|(^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$)|(^\d{6,8}$)/ ,
		cvv: /[0-9]{3}/ ,
		ccno: /[0-9]{16}/
	});
	$('#frmOrder').h5Validate({
		errorClass:'formError'
	});
	
	$("input[data-mask='phone']").mask("999-999-9999");
	
	$('#agree').change(function(){
		if($(this).is(":checked")) {
			$(this).parent().removeClass('formError');
		} else {
			$(this).parent().addClass('formError');
		}
	});
	
	if($('input[name="billingSameAsShipping"]').is(":checked")) {
		$('.billingDiv').hide();
	} else {
		$('.billingDiv').show();
	}
	
	$('.ToggleAddress').click(function(e) {
        $('.billingDiv').slideToggle();
    });
	
	var btnclicked= "";
	$('.btn-submit').click(function(e) {
        btnclicked = $(this).attr('name');
    });
	
	$("#frmOrder").submit(function(e) {
         var result = $('#frmOrder').h5Validate('allValid');
		 if(result){
			 if(btnclicked == 'btn1'){
				$('.step1').hide();
				$('.step2').show();
				return false;
			 }else{
				 $('#frmOrder input[type="submit"]').prop('disabled', true);
				window.internalLink = true;
				showProcessing();
				return true;

			 }
		 }else{ 
			$("input[type='checkbox'].formError").each(function(){ 
				$(this).parent().addClass('formError');
			});
			event.preventDefault();
			return false;
			 
		 }
		 
    });
	
	$(document).on('click','.discountBtn', function(){
		if($('#coupon_code').is(":disabled")) {
			return false;
		}
		
		
		//$(do)
		
		//alert($(document).find("input[name='promoCode']").filter(':visible:first').val());
		if($(document).find("input[name='promoCode']").filter(':visible:first').val() == '') {
			alert('Enter Promo Code (if you have one) to avail discount!');
		
		} else {
			applyCoupon($(document).find("input[name='promoCode']").filter(':visible:first').val());
		}
		return false;
	});
});
$("#chk-box").click(function(){ 
	$(".billing-info").slideToggle('slow');
});

function showTotalDisPrice(dper){
	
	var total = 0;
	var shipping = 0.00;
	var discount = dper;
	var discount_price = 0;
	var grand_total = 0;
	var sub_total_price = 0;
	var sub_total = $("#total-price").text().split("$").pop();
	shipping = $("#shipping-total").text().split("$").pop();
	sub_total = parseFloat(sub_total);
	shipping = parseFloat(shipping);
	total = sub_total + shipping;
	discount_price = (total * (discount / 100));
	grand_total = sub_total + shipping  - discount_price;
	$("#disprc").html('- $' + discount_price.toFixed(2) );
	$('.dis-charge').show();
	//$("#total-price").html('$' + sub_total_price.toFixed(2) );
	$("#grand-total").html('$' + grand_total.toFixed(2));
	
}

function applyCoupon(ccd,frm) { 
	
	$.ajax({
		url: 'ajax/coupserv.php',
		type: 'POST',
		data: {
			ccode: ccd
		},
		dataType: 'json',
		success: function(json) {
			if (json.stat && json.stat == 'ERROR') {
				alert(json.content);
				$('#promoCode').focus();
			} else if (json.stat && json.stat == 'SUCCESS') {
				var dper = parseFloat(json.content.p);
				$('#promoCode').val(json.content.c);
				$('#promoCode').attr('disabled', true);
				//$(this).find('input[type=text]').filter(':visible:first').attr('disabled', true);
				//$('.coupon_text').attr('disabled', true);
				//$('.coupon_text').val(ccd);
				$('.discountBtn').attr('disabled', true);
				//$('#coupon-txt-container').html('<small>' + json.content.c + '</small>');
				$('.discountBtn').val('APPLIED');
				//$('.discountBtn').css('color', '#fff');
				showTotalDisPrice(dper);
			}
		}
	});
}
</script>
</body>
</html>

