<!--============= HEADER =============-->

<?php include("header.php");?>

<!--============= COMMON HEADER =============-->
<div class="commen-banner">
    <div class="container">
    <ul class="brdcmb">
          <li><a href="index.php">Home</a></li>
          <li> Thank</li>
        </ul>
    	<p class="common-head">Thank You</p>
    </div>
</div>

<!--=============THANKYOU SECTION =============-->

<div style="float:left; width:100%; text-align:center;" id="section2">
    <div class="container">
        <div>
            <p class="topbnrtxt2-pr" style="text-align:center; width:100%; color: #f44336;font-size:30px; margin-top:40px;">Thank You For Your Order</p>
            <div class="thanks-box" >
                <h3 style="padding:35px 0 0 0;">Order Id : <span style="color:#F00;font-weight:bold;">12345</span></h3>
                <h3 style="padding:27px 0 10px 0;">Hours of Operation</h3>
                <p style="text-align:center; line-height:28px;"> <?php echo $cshour ?></p>
                <h3 style="padding:28px 0 0 0;">Email</h3>
                <p style="text-align:center;"><?php echo $config['company_email'];?></p>
        	</div>
        </div>
    </div>
</div>

<!--============= FOOTER =============-->

<?php include 'footer.php'; ?>
</div>
</body>
</html>
