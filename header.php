<?php 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
?>
<?php  include "config.inc.php";?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $config['product_name']?></title>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/fonts.css" type="text/css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noticia+Text:wght@400;700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div class="top-sec">
	<div class="container">
        <div class="navigation-sec">
            <a href="index.php" class="logo"><img src="images/<?php if($activePage == "index"){echo 'logo.png';} else{echo 'logo.png';}?>" ></a>
            <ul class="nav-bar">
                <li><a href="index.php" class="<?php if($activePage == "index"){echo 'active';}?>">Home</a></li>
                <li><a href="products.php" class="<?php if($activePage == "products"){echo 'active';}?>">Products</a></li>
                <li><a href="cart.php" class="<?php if($activePage == "cart"){echo 'active';}?>">Cart</a></li>            
                <li><a href="contact.php" class="<?php if($activePage == "contact"){echo 'active';}?>">Contact Us</a></li>
            </ul>
        </div>
	</div>
</div>

