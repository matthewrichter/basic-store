<?php 
$prods1 = '1-2-3-4';
if(!empty($_GET["pidx"])){
	$prods = explode("-",trim(base64_decode($_GET["pidx"])));
}else{
	$prods = explode("-",trim($prods1));
}
?>
<!--============= HEADER =============-->

<?php include("header.php");?>

<div class="commen-banner">
    <div class="container">
    	 <ul class="brdcmb">
          <li><a href="index.php">Home</a></li>
          <li> Products</li>
          <li><?php echo $config['productSpecs'][$prods[0]]['nm']; ?></li>
        </ul>
      <p class="common-head"><?php echo $config['productSpecs'][$prods[0]]['nm']; ?></p>
    
    </div>
</div>

<p class="clearall"></p>
<!--============= COMMON HEADER =============-->

<!--============= PRODUCT DETAILS =============-->

<div class="prd-details">
	<div class="container">
        	<p class="cmn-bdr"></p>
            <div class="clearall"></div>
            <div class="prod-top"></div>
            <div class="prds-left">
                <img src="<?php echo $config['productSpecs'][$prods[0]]['img']; ?>" class="prd-dtl-1">
            </div>
            <div class="prds-right add-cart-blk">
                <p class="prd-dtl1">
					<!--<?php echo $config['product_name']?>-->
                	<span><?php echo $config['productSpecs'][$prods[0]]['nm']; ?></span>
                    <img src="images/star.png" alt="">
                </p>
                <p class="prd-dtl3"><?php echo $config['productSpecs'][$prods[0]]['desc']; ?></p> 
                <!--<ul class="prg-bar">
                    <?php echo $config['productSpecs'][$prods[0]]['desc_two']; ?>
                </ul>-->
                <?php $prodopt= $config['productSpecs'][$prods[0]]['product-type'];
                if($prodopt=="multi"){?>
          <div class="prod-option">
            <p class="options"><span>Select Option Type:</span></p>
            <ul>
              <li>
                <?php  if(!empty($prods[0]) && array_key_exists($prods[0], $config['productSpecs'])){ ?>
                <input type="radio" class="add-cart-pid" name="prod-opt" id="pack1" data-id="<?php echo $config['productSpecs'][$prods[0]]['id']; ?>" onClick="section('trial-order')" value="trial-order" data-price="<?php echo number_format($config['productSpecs'][$prods[0]]['prc'],2); ?>">
                <label for="pack1">Try Before You Buy</label>
                <?php } ?>
              </li>
              <li>
                <?php  if(!empty($prods[0]) && array_key_exists($prods[1], $config['productSpecs'])){ ?>
                <input type="radio" class="add-cart-pid" name="prod-opt" data-id="<?php echo $config['productSpecs'][$prods[1]]['id']; ?>" id="pack2" onClick="section('straightsale-order')" value="straightsale-order" data-price="<?php echo number_format($config['productSpecs'][$prods[1]]['prc'],2); ?>">
                <label for="pack2">One Time Purchase</label>
                <?php } ?>
              </li>
            </ul>
          </div>
          <div class="trial-option">
            <ul id="section1_fields" class="section1_fields">
              <li >10 Day Trial <span>$0.00</span></li>
              <li >Sub Total <span>$0.00</span></li>
              <li >Shipping <span id="shipping-price">$<?php echo $config['productSpecs'][7]['shp-prc']; ?></span></li>
              <li >Total <span id="total-price">$<?php echo $config['productSpecs'][7]['shp-prc']; ?></span></li>
            </ul>
            <div id="" class="section1_fields section2_fields"><?php echo $config['productSpecs'][7]["chkterms"]; ?></div>
          </div>
          <div class="one-time-option">
            <div id="section4_fields" class="section1_fields">
              <li>One Time Purchase <span>$<?php echo $config['productSpecs'][8]['prc']; ?></span></li>
              <li>Sub Total <span>$<?php echo $config['productSpecs'][8]['prc']; ?></span></li>
              <li>Shipping <span id="shipping-price">$0.00</span></li>
              <li>Total <span id="total-price">$<?php echo $config['productSpecs'][8]['prc']; ?></span></li>
            </div>
            <div class="section1_fields section2_fields"> *You will be charged the full price of $<?php echo $config['productSpecs'][8]["prc"]; ?>. If you are for any reason unsatisfied with our product, you can send back any used or unused products for a full refund by calling <?php echo $config['toll_free'];?>. Shipping and handling costs are free for one-time purchases. You have 30 days to return the product for a full refund, and you will be responsible for applicable postage charges.</div>
          </div>
          <a href="javascript:void(0);" class="add-to-cart-mul cart-img s1-ord-btn">Add To cart</a><img src="images/cards.png" alt="" class="card">
          <?php }else {?>
          <div class="pric-box">
            <p class="pro-dtil-rgt-p2"> <span id="pid_prc" class="price" data-id="<?php echo $config['productSpecs'][$prods[0]]['id']; ?>" data-price="<?php echo number_format($config['productSpecs'][$prods[0]]['prc'],2); ?>"> Price: $<?php echo number_format($config['productSpecs'][$prods[0]]['prc'],2); ?> </span> </p>
          </div>
          <a href="javascript:void(0);" class="add-to-cart cart-img s1-ord-btn">Add To cart</a><img src="images/cards.png" alt="" class="card">
          <?php  }?>
                <!--<div class="pric-box">
                    <p class="pro-dtil-rgt-p2">
                         <span id="pid_prc" class="price" data-id="<?php echo $config['productSpecs'][$prods[0]]['id']; ?>" data-price="<?php echo number_format($config['productSpecs'][$prods[0]]['prc'],2); ?>">Price: $<?php echo number_format($config['productSpecs'][$prods[0]]['prc'],2); ?>
                        </span>
                    </p>
                </div>
                <a href="javascript:void(0);" class="add-to-cart s1-ord-btn">Add To Cart </a>
                <img src="images/cards.png" alt="" class="card">-->
            </div>
        </div>
    </div>
</div>

<!--============= PRODUCT SECTION =============-->

<div class="prod-mid-sec">
    <div class="container">
       <p class="s1-btm-p3"><span>Our Other </span>Recommended Products </p>
       <p class="clearall"></p>
        <div class="clearall"></div>
        <div class="s3-block p-blk">
			<?php if(!empty($prods[0]) && $prods[0] != 1){ ?>
        	<div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][1]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][1]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][1]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('1');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 2){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][2]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][2]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][2]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('2');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 3){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][3]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][3]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][3]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('3');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 4){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][4]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][4]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][4]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('4');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 5){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][5]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][5]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][5]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('5');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 6){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][6]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][6]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][6]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('6');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 7 && $prods[0] != 8){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][8]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][8]['nm']; ?></p>
                  <div class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][8]['prc']; ?></span>
          <p class="s3-prod1"><?php echo $config['productSpecs'][7]['altnm']; ?></p></div>
                  <a href="shop.php?pidx=<?php echo base64_encode('7-8');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
            <?php if(!empty($prods[0]) && $prods[0] != 9){ ?>
            <div class="s3-box2 item" >
               
                <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][9]['img']; ?>" class="s3-prod-img1"> </div>
                <div class="s3-box2-rgt">
                <img src="images/star.png" class="star">
                  <p class="s3-box2-txt1"><?php echo $config['productSpecs'][9]['nm']; ?></p>
                  <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][9]['prc']; ?></span></p>
                  <a href="shop.php?pidx=<?php echo base64_encode('9');?>" class="s1-ord-btn">Place Order!</a> </div>
              </div>
            <?php } ?>
        </div>
    </div>
</div>

<!--============= BOTTOM STRIP =============-->

<!--============= FOOTER =============-->

<?php include 'footer.php'; ?>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/cartv5.js.php"></script>
<script src="js/bookmarkscroll.js"></script> 
<script src="js/wan-spinner.js"></script>
<script type='text/javascript'>
$(document).ready(function(e) {
   $('.plus').click(function(){
		var price = +$(".price").data("price"); 
		var qtybox = +$(".add-cart-blk").find("input[name='qtybox']").val()+1;
		$("#pid_prc").html("$" + parseFloat(price * qtybox).toFixed(2));
	});
	$('.minus').click(function(){
		var price = +$(".price").data("price"); 
		var qtybox = +$(".add-cart-blk").find("input[name='qtybox']").val()+1;
		$("#pid_prc").html("$" + parseFloat((price * qtybox) - price).toFixed(2));
	});
	
    $(".add-to-cart").click(function(e){
			e.preventDefault();
			var condiv = $(this).closest('.add-cart-blk');
			var pid = condiv.find("#pid_prc").attr("data-id");
			console.log(pid);
			addItem(pid, 1);
		});
		
		$(".add-to-cart-mul").click(function(e){
			e.preventDefault();
			var condiv = $(this).closest('.add-cart-blk');
			if(condiv.find("input:radio:checked").size()>0){
				if($('.add-cart-pid:checked').size() > 0){
					var pid = $('.add-cart-pid:checked').attr("data-id");
				removeItem(1007, 1);	
				removeItem(1008, 1);
				addItem(pid, 1);
				}
			}else{
				alert("Please select a product.");
			}
		});
	
	var options = {
	  maxValue: 10,
	  minValue: 1,
	  step: 1,
	  inputWidth: 30,
	  start: 1,
	  plusClick: function(element, val) {
		//console.log(val);
	  },
	  minusClick: function(element, val) {
		//console.log(val);
	  },
	  exceptionFun: function(val) {
		//console.log("excep: " + val);
	  },
	  valueChanged: function(element, val) {
		var prd_price = $('#pid_prc').data("price");
		  console.log(prd_price + " --price");
		  var total_qty = $('.qtybox').val();
		  console.log(total_qty + " --qty");
		  var totat_price = prd_price * total_qty;
		  console.log(totat_price + " --total");
		  $('#pid_prc').html("$" +  totat_price.toFixed(2));
	  }
	}
	$(".wan-spinner2").WanSpinner(options).css("border-color", "#e1e1e1");	
});
	
</script>
<script>
	var acc = document.getElementsByClassName("accordion");
	var i;
	
	for (i = 0; i < acc.length; i++) {
	  acc[i].addEventListener("click", function() {
		this.classList.toggle("active");
		var panel = this.nextElementSibling;
		if (panel.style.maxHeight){
		  panel.style.maxHeight = null;
		} else {
		  panel.style.maxHeight = panel.scrollHeight + "px";
		} 
	  });
	}
	
	var items = document.querySelectorAll(".item"), index, i;
	var min = 0;
	var max = items.length - 1;
	var count = items.length - 3;
	
	while(count > 0) {
		index = Math.floor(Math.random() * (max - min + 1)) + min;
		if(items[index].style.display != "none") {
			items[index].style.display = "none";
			--count;
		}
	}
</script>
<script> 
 function section(chk){
	 
	 if(chk =="trial-order"){
		 $('.trial-option').show(100);
		 $('.one-time-option').hide(100);
	 }
	 if(chk =="straightsale-order"){
		 $('.one-time-option').show(100);
		 $('.trial-option').hide(100);
		 //$('#section4_fields').show(100);
		// $('#section5_fields').show(100);
	 }
 }
</script>
</body>
</html>
