
<form role="form" action="thankyou.php" id="frmOrder" method="post">
    <input type="hidden" name="step" value="direct_multi_product" />
    <input type="hidden" name="campaignId" value="" />
    <input type="hidden" name="shippingId" value="" autocomplete="off" />
    <input type="hidden" name="ipAddress" value="" />
    <input type="hidden" id="cc_expires" name="cc_expires" value="">
	<div class="frm_con_bgcolr">

        <!--************************ORDER SUMMERY*************************************-->
        
        <div class="frm-container cart-smmry">
            <div class="inner-container">
                <div id="cartBox">
                    You have not selected any product... &nbsp <a id='browse' href='index.php'>Browse Products</a>
                </div>
            </div>
        </div>
        
        <!--******************SHIPPING INFO FORM*******************-->
        
        <div class="frm-container">
            <div class="shipping_heading">
                <h3>Shipping Information</h3>
            </div>
            <div class="inner-container">
            	<div class="frm-container-inner">
                <div class="label small">
                    <i class="fa fa-user"></i><p>First Name</p>
                    <input type="text" name="firstName" placeholder="First Name" id="firstName" value="" required maxlength="64" />
                </div>
                <div class="label small flr-r">
                    <i class="fa fa-user"></i><p>Last Name</p>
                    <input type="text" name="lastName" id="lastName" placeholder="Last Name" value="" required maxlength="64" />
                </div>
                <div class="label">
                    <i class="fa fa-map-marker"></i><p>Address</p>
                    <input type="text" name="shippingAddress1" id="shippingAddress1" placeholder="Address" value="" required maxlength="64" />
                </div>
                
                <div class="label small flr-l">
                    <i class="fa fa-building"></i><p>Country</p>
                    <select name="shippingCountry" id="shippingCountry" required >
                        <option value="">Select Country</option>
                        <option value="US">United States</option> 
                    </select>
                    <input type="hidden" name="shippingCountry" id="shippingCountry" value="US" />
                </div>
                <div class="label small flr-r">
                    <i class="fa fa-building"></i><p>State</p>
                    <select name="shippingState" id="shippingState" required >
                        <option value="">Select State</option>
                    </select>
                    <input type="hidden" name="shippingCountry" id="shippingCountry" value="US" />
                </div>
                <div class="label small">
                    <i class="fa fa-building"></i><p>City</p>
                    <input type="text" name="shippingCity" id="shippingCity" placeholder="City" value="" required maxlength="32" />
                </div>
                <div class="label small flr-r">
                    <i class="fa fa-binoculars"></i><p>Zip</p>
                    <input type="text" name="shippingZip" id="shippingZip" placeholder="Zip" value="" required maxlength="5" class="h5-zip"  />
                </div>
                
                <div class="label small">
                    <i class="fa fa-envelope"></i><p>Email</p>
                    <input type="email" name="email" id="email" placeholder="Email" value="" required maxlength="96" class="h5-email" />
                </div>
                <div class="label small flr-r">
                    <i class="fa fa-phone"></i><p>Phone</p>
                    <input type="text" name="phone" id="phone" placeholder="Phone" value="" required maxlength="12" class="h5-phone" data-mask="phone" />
                </div>
            </div>
			</div>          
        </div>
        
		<!--******************PAYMENT INFORMATION*************8-->
        <div class="frm-container">
            
            <div class="shipping_heading" style="margin-left: 0px;">
                <h3>Payment Information</h3>
            </div>
            <div class="inner-container">
            	<div class="frm-container-inner">
                <div class="is_bill">
                    <input type="checkbox" name="billingSameAsShipping" class="ToggleAddress" value="yes" checked="checked" >
                    Billing address is the same as shipping?
                </div>
                <div class="cards">
                    <span>We Accepts</span><img src="images/cards.png"  />
                </div>
                <div id="billingDiv" class="billingDiv" style="float:left; width:100%;">
                    <div class="label">
                        <i class="fa fa-user"></i><p>First Name</p>
                        <input type="text" name="billingFirstName" id="billingFirstName" placeholder="First Name" value="" required maxlength="64" />
                    </div>
                    <div class="label">
                        <i class="fa fa-user"></i><p>Last Name</p>
                        <input type="text" name="billingLastName" id="billingLastName" placeholder="Last Name" value="" required maxlength="64" />
                    </div>
                    <div class="label">
                        <i class="fa fa-map-marker"></i><p>Address</p>
                        <input type="text" name="billingAddress1" id="billingAddress1" placeholder="Address" value="" required maxlength="64" />
                    </div>
                    <div class="label">
                        <i class="fa fa-building"></i><p>City</p>
                        <input type="text" name="billingCity" id="billingCity" placeholder="City" value="" required maxlength="32" />
                    </div>
                    <div class="label">
                        <i class="fa fa-building"></i><p>State</p>
                        <select name="billingState" id="billingState" required  >
                            <option value="">Select State</option>
                        </select>
                        <input type="hidden" name="billingCountry" id="billingCountry" value="US" />
                    </div>
                    <div class="label">
                        <i class="fa fa-binoculars"></i><p>Zip</p>
                        <input type="text" name="billingZip" id="billingZip" placeholder="Zip" value="" required maxlength="8" class="h5-zip" />
                    </div>
                </div>
                <div class="label">
                    <i class="fa fa-credit-card"></i><p>Card Type</p>
                    <select name="fields_expmonth" id="fields_expmonth" class="" required>
                        <option selected="" value="">Card Type</option>
                        <option value="visa">Visa</option>
                        <option value="master">MasterCard</option>
                        <option value="discover">Discover</option>
                    </select>
                </div>
                <div class="label">
                    <i class="fa fa-credit-card"></i><p>Card Number</p>
                    <input type="text" name="creditCardNumber" id="creditCardNumber" value="" placeholder="Enter Your Card No." autocomplete="off" required maxlength="16" onkeydown="return onlyNumbers(event,'cc')" class="h5-ccno" />
                </div>
                <div class="label"><p>Expiration Date :</p></div>
                <div class="label small-s">
                    <i class="fa fa-calendar"></i><p>Month</p>
                    <select name="fields_expmonth" id="fields_expmonth" class="" onchange="javascript:update_expire()" required>
                        <option selected="" value="">Month</option>
                        <option value="01">(01) January</option>
                        <option value="02">(02) February</option>
                        <option value="03">(03) March</option>
                        <option value="04">(04) April</option>
                        <option value="05">(05) May</option>
                        <option value="06">(06) June</option>
                        <option value="07">(07) July</option>
                        <option value="08">(08) August</option>
                        <option value="09">(09) September</option>
                        <option value="10">(10) October</option>
                        <option value="11">(11) November</option>
                        <option value="12">(12) December</option>
                    </select>
                </div>
                <div class="label small-s flr-r">
                    <i class="fa fa-calendar exp-year"></i><p>Year</p>
                    <select name="fields_expyear" id="fields_expyear" class="" onchange="javascript:update_expire()" required>
                        <option selected="" value="">Year</option>
                        <?php $y=(integer) date('y'); for($i = $y; $i <= $y+9; $i ++) echo'<option value=\''.$i.'\'>20'.$i.'</option>'?>
                    </select>
                </div>
                <div class="label">
                    <i class="fa fa-credit-card"></i><p>CVV</p>
                    <input autocomplete="off" maxlength="3" type="text" id="cc_cvv" name="CVV" required onkeydown="return onlyNumbers(event,'phone')" class="small h5-cvv" placeholder="CVV:" style="float:left;" /> 
                    <a href="images/cvv2-location.png" onClick="window.open('images/cvv2-location.png','whatsthis','scrollbars=yes,width=600,height=200');return(false);">What is this ?</a>
                   
                </div>
                <div class="cart-terms">
                    <input type="checkbox" name="agree" id="agree" value="Y" class="chk radio" required="required" ><span class="trm_1"></span><br />
                </div>
                <center>
                    <input type="submit"  value="COMPLETE CHECKOUT"  class="submit-btn"/>
                </center>
            </div>
            </div>
        </div>
	</div>    
    <div class="clearall"></div>
</form>
