<div id="footer">
  <div class="container"> <img src="images/logo.png" class="logo-f">
    <div class="foter-sec2" >
      <ul class="footer-list1">
        <li><?php echo $config['company_address'];?></li>
      </ul>
      <div class="ftr-call">
        <p><span class="material-icons">call</span> <?php echo $config['toll_free']?></p>
        <p><span class="material-icons">mail</span> <?php echo $config['company_email']?></p>
      </div>
    </div>
  </div>
  <div class="foter-sec2">
    <ul class="footer-list">
      <li><a href="terms.php" target="_blank">Terms &amp; Conditions</a></li>
      <li><a href="privacy.php" target="_blank">Privacy Policy</a></li>
      <li><a href="contact.php">Contact Us </a></li>
      <li><a href="cancel_auto-renewal.php" target="_blank">Cancel Auto-Renewal </a></li>
    </ul>
  </div>
  <div class="foter-sec1">
    <p class="ft-lg-txt">© Copyright <?php echo date('Y').' '. $config['company_name'] ?>. All Rights Reserved</p>
  </div>
</div>
</div>
