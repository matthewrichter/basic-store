<!--============= HEADER =============-->
<?php include("header.php"); ?>
<!--============= COMMON HEADER =============-->
<div class="commen-banner">
  <div class="container">
       <ul class="brdcmb">
          <li><a href="index.php">Home</a></li>
          <li> Terms and Conditions</li>
        </ul>
      <p class="common-head">Terms and Conditions</li>
  </div>
</div>
<div class="trms-box innr_bg">
  <div class="container">
    <ul class="term-link">
      <li>CLICK HERE <a href="terms-trial.php#trial">Try Before You Buy - Terms and Conditions</a></li>
    </ul>
    <div class="innr_bg_div">
      <div>
        <h2>CAREFULLY READ AND UNDERSTAND THESE TERMS BEFORE ORDERING ANY PRODUCT THROUGH THIS WEBSITE.</h2>
        <p><strong>ATTENTION:</strong> This is a legal agreement (the "Agreement") between You, the individual, company or organization ("you," "your," or "Customer") and <a href="mailto:<?php echo $config['company_email'] ?>"><?php echo $config['company_email'] ?></a> ("we," "our", "Company"). By ordering, accessing, using or purchasing http://<?php echo $config['site_url'];?>/("Product") through this website or related websites (collectively the "Website"), you are agreeing to be bound by, and are becoming a party to, this Agreement. We may at our sole and absolute discretion change, add, modify, or delete portions of this Agreement at any time without notice. It is your sole responsibility to review this Agreement for changes prior to use of the Website or purchase of the Product.</p>
        <p>IT IS STRONGLY RECOMMENDED THAT YOU REVIEW THIS DOCUMENT IN ITS ENTIRETY BEFORE ACCESSING, USING OR BUYING ANY PRODUCT THROUGH THE WEBSITE AND PRINT A COPY FOR YOUR RECORDS.</p>
      </div>
      <div>
        <h2>Terms & Conditions</h2>
        <p>Please carefully read the following terms and conditions as when you purchase any of the products from our web site, you agree and are bound to the following terms and conditions. Your order will not be processed unless you check the box near the terms and conditions signifying that you have read and agreed to the terms.</p>
        <p>This Agreement is between our Company and you ("you" or "Customer"). This Section sets forth the terms and conditions which apply to the use by you of the website (as defined below) and any other product or service offered for sale by us and/or our affiliates.</p>
        <p>The right to use any product or service offered by our Company is personal to you and is not transferable to any other person or entity. We reserve the right to make changes to the website, policies, and these terms at any time without notice.</p>
      </div>
      <div>
        <h2>Ordering & Shipping</h2>
        <p>By placing an order, you will be charged the full price of the package you selected and the order will be shipped to the address you provided during checkout. All products ship within 1 business day and deliver within 2-4 days from shipment.</p>
      </div>
      <div>
        <h2>Indemnification</h2>
        <p>You agree to defend, indemnify and hold harmless the Company, its affiliates and their respective directors, officers, employees and agents from and against all claims and expenses, including attorneys' fees, arising out of the use by you of the website, including claims by other users, access, products.</p>
      </div>
      <div>
        <h2>One Time Purchase terms:</h2>
        <p><strong><?php echo $config['productSpecs'][8]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][8]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][8]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][8]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][1]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][1]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][1]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][1]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][2]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][2]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][2]['nm'] ?>. If you are not completely satisfied with the <?php echo $config['productSpecs'][2]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][3]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][3]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][3]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][3]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][5]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][5]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][5]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][5]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][4]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][4]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][4]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][4]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][6]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][6]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][6]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][6]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <p><strong><?php echo $config['productSpecs'][9]['nm'] ?></strong></p>
        <p>By placing an order with us you will be charged $<?php echo $config['productSpecs'][9]['prc'] ?> + $0.00 for S&H one time for a <?php echo $config['productSpecs'][9]['nm'] ?>. If you are not completely satisfied with <?php echo $config['productSpecs'][9]['nm'] ?> at any time, please call <?php echo $config['toll_free'] ?>, <?php echo $cshour;?>. You will receive your product within 2-4 business days of payment. I agree that my credit card charge will appear as <?php echo $card_statement ?>.</p>
        <h2>Return Policy</h2>
        <p>We believe in complete customer satisfaction. Eligible refund 30 days from receipt of product. We reserve the right to refuse a refund to any customer who repeatedly requests refunds or who, in our judgment, requests refunds in bad faith.</p>
        <p>To return a product, you will need to obtain a Return Merchandise Authorization (RMA) number by contacting our customer service team on <?php echo $config['toll_free'] ?> or by emailing at <a href="mailto:<?php echo $config['company_email'] ?>"><?php echo $config['company_email'] ?></a>. Our customer service department will email you an RMA number within 24-48 hours of your request. You will need to send the product back to the address provided below. Upon receipt you will receive a full refund of purchase price, minus the shipping, handling or other charges. All returns are subject to a restocking fee. Refused or returned packages that are sent to us without prior approval from Customer Care are not eligible for a refund.</p>
        <p>Our call center is open <?php echo $cshour;?>.</p>
      </div>
      <div>
        <h2>Return Address:</h2>
        <p>If you are not satisfied and want to return the product, please write an email with your location to <a href="mailto:<?php echo $config['company_email'] ?>"><?php echo $config['company_email'] ?></a> and we will provide you with the address of the nearest return center.</p>
        <p><strong>Be sure to write your RMA # on the outside of the envelope for proper account credit.</strong></p>
        <p>We are not responsible for lost or stolen items. We recommend all returned items to be sent using some type of delivery confirmation system to ensure proper delivery.</p>
        <p>After the shipping department receives your return, it generally takes 7 business days or sooner to process your refund. Once a return is processed, it usually takes 3-5 business days for the return to be posted to your account, depending on your financial institution.</p>
        <p>(Fulfillment House)<br>
          <?php echo $config['company_address'];?>
        <ul>
          <li>
            <p>Packages marked "Return to Sender" will NOT be processed or refunded. Returned packages will only be refunded with an RMA number that was provided by Customer Service. Call Customer Service at <?php echo $config['toll_free'] ?> for your RMA number. RMA numbers remain valid for 30 days.</p>
          </li>
          <li>
            <p>Refunds will only be issued to the same form of payment originally used for purchase.</p>
          </li>
          <li>
            <p>Customer is responsible for return shipping charges.</p>
          </li>
          <li>
            <p>After the warehouse receives your return, it generally takes 7 business days to process your return. Please keep in mind that your bank typically posts credit in the billing cycle in which it was received. Therefore, the number of days it takes for credit to post to your account may vary, depending on your banking institution's billing and credit schedule.</p>
          </li>
          <li>
            <p>Our customer service representatives may offer you a discounted price or partial refund if you call to cancel your account. You may accept or reject the discounted price or partial refund. If you reject the discount or partial refund, then in order to obtain your refund you will be required to return the product to us using an RMA number as described above. If you accept the discount or partial refund, you agree to waive your right to return this order.</p>
          </li>
        </ul>
        </p>
      </div>
      <div>
        <h2>Reversals & Chargebacks</h2>
        <p>We consider Chargebacks and reversals as potential cases of fraudulent use of our services and/or theft of services, and will be treated as such. We reserve the right to file a complaint with the appropriate local and federal authorities to investigate. Be advised that all activity and IP address information is being monitored and that this information may be used in a civil and/or criminal case(s) against a client if there is fraudulent use and or theft of services.</p>
      </div>
      <div>
        <h2>Representations</h2>
        <p>You hereby represent and warrant that:</p>
        <ol>
          <li>You are age eighteen (18) or older.</li>
          <li>You have read this Agreement and thoroughly understand the terms contained.</li>
          <li>You further represent that our Company has the right to rely upon all information and may contact you by email, telephone or postal mail for any purpose regarding the use of this website.</li>
        </ol>
      </div>
      <div>
        <h2>Billing & Customer Support</h2>
        <p>We welcome all support inquires by the following methods:</p>
        <p>Phone: <?php echo $config['toll_free'] ?></p>
        <p><a href="mailto:<?php echo $config['company_email'] ?>"><?php echo $config['company_email'] ?></a> </p>
        <p><?php echo $config['company_email'] ?><br>
          <?php echo $config['company_address'] ?></p>
      </div>
    </div>
  </div>
</div>
</body>
</html>