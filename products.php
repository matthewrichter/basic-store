<!--============= HEADER =============-->
<?php include("header.php"); ?>
<!--============= COMMON HEADER =============-->
<div class="commen-banner">
  <div class="container">
       <ul class="brdcmb">
          <li><a href="index.php">Home</a></li>
          <li> Products</li>
        </ul>
      <p class="common-head">Our Products</li>
  </div>
</div>
<!--============= PRODUCT SECTION =============-->
<div class="section3">
  <div class="container" id="001">

    <div class="clearall"></div>
    <div class="s3-block position">
    	<div class="s3-box2 featrd-prod">
        
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][5]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][5]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][5]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('5');?>" class="s1-ord-btn"> Place Order!</a> </div>
      </div>
    	<div class="s3-box2">
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][8]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][8]['nm']; ?></p>
          <div class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][8]['prc']; ?></span>
          <p class="s3-prod1"><?php echo $config['productSpecs'][7]['altnm']; ?></p></div>
          <a href="shop.php?pidx=<?php echo base64_encode('7-8');?>" class="s1-ord-btn"> Place Order </a> </div>
      </div>
      <div class="s3-box2" >
       
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][1]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][1]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][1]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('1');?>" class="s1-ord-btn"> Place Order! </a> </div>
      </div>
      <div class="s3-box2">
      
        <div class="pro1-bg3"><img src="<?php echo $config['productSpecs'][2]['img']; ?>" class="s3-prod-img1"></div>
        <div class="s3-box2-rgt2">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][2]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][2]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('2');?>" class="s1-ord-btn">Place Order!</a> </div>
      </div>
      <div class="s3-box2">
        
        <div class="pro1-bg2"> <img src="<?php echo $config['productSpecs'][3]['img']; ?>" alt="" class="s3-prod-img1" /> </div>
        <div class="s3-box2-rgt">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][3]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][3]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('3');?>" class="s1-ord-btn"> Place Order!</a> </div>
      </div>
      <div class="s3-box2">
        
        <div class="pro1-bg3"> <img src="<?php echo $config['productSpecs'][4]['img']; ?>" class="s3-prod-img1" > </div>
        <div class="s3-box2-rgt2">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][4]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][4]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('4');?>" class="s1-ord-btn"> Place Order!</a> </div>
      </div>
      
      <div class="s3-box2">
        
        <div class="pro1-bg3"> <img src="<?php echo $config['productSpecs'][6]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt2">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][6]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][6]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('6');?>" class="s1-ord-btn"> Place Order!</a> </div>
      </div>
      <div class="s3-box2">
        
        <div class="pro1-bg3"> <img src="<?php echo $config['productSpecs'][9]['img']; ?>" class="s3-prod-img1"> </div>
        <div class="s3-box2-rgt2">
        <img src="images/star.png" class="star">
          <p class="s3-box2-txt1"><?php echo $config['productSpecs'][9]['nm']; ?></p>
          <p class="s3-box2-txt3">Price: <span>$<?php echo $config['productSpecs'][9]['prc']; ?></span></p>
          <a href="shop.php?pidx=<?php echo base64_encode('9');?>" class="s1-ord-btn">Place Order! </a> </div>
      </div>
    </div>
  </div>
</div>

<!--============= BOTTOM STRIP =============--> 

<!--============= FOOTER =============-->

<?php include 'footer.php'; ?>
</body>
</html>
