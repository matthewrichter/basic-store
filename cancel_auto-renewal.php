<!--============= HEADER =============-->
<?php include("header.php"); ?>
<!--============= COMMON HEADER =============-->
<div class="commen-banner">
  <div class="container">
       <ul class="brdcmb">
          <li><a href="index.php">Home</a></li>
          <li> Cancel Auto Renewal</li>
        </ul>
      <p class="common-head">Cancel Auto Renewal</li>
  </div>
</div>

<div class="innr_bg auto-renew">
  <div class="container">
    <div class="pop_form">
        <p class="cancel_hed">Order Cancellation Form</p>
        <p class="cancel_text">Please fill out the information below to cancel your order.</p>
        <div class="cancel_from">
            <form id="target" action="" method="post">
                <div class="from_elemnt fromelements_half_l">
                    <label>First Name:</label>
                    <input type="text" class="validate[required]">
                </div>
                <div class="from_elemnt fromelements_half_r">
                    <label>Last Name:</label>
                    <input type="text" class="validate[required]">
                </div>
                <div class="from_elemnt fromelements_half_l">
                    <label>Email:</label>
                    <input type="text" class="validate[required]">
                </div>
                <div class="from_elemnt fromelements_half_r">
                    <label>Last 4 #'s of Credit Card:</label>
                    <input type="text" class="validate[required]">
                </div>
                
                <div class="from_elemnt">
                    <label>Comments:</label>
                    <textarea  class="validate[required]"> I am requesting to terminate the auto-renewal for my monthly subscription for your products.</textarea>
                </div>
                <button class="btn odr-btn s1-ord-btn">Submit the form</button>
            </form>
        </div>
    </div>
    </div>
</div>
<script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		
    jQuery(document).ready(function (){
		jQuery("#target").validationEngine('attach', {
			onValidationComplete: function(form, status){
				if(status){ 
					showProcessing();
					jQuery("#formID")[0].submit();
				}
			}  
		});
	});
var currencySymbol = '$';
</script>
</body>
</html>