<?php

require_once('../coupon.inc.php');

header("Content-Type: application/json");

function showError($msg = 'Error Occurred!!') {
	echo json_encode(array('stat' => 'ERROR', 'content' => $msg));
	exit(0);
}

if(!isset($_POST['ccode']) || trim($_POST['ccode']) == '') {
	showError('Enter Promo Code (if you have one) to avail discount!');
}

$promo_code = $_POST['ccode'];
$promo_code = substr(trim($promo_code), 0, 50);
$promo_code = preg_replace("/[^a-zA-Z0-9\-\_\#\.\ ]/", '', $promo_code);

$coupon = array();
foreach($config['coupons'] as $coup) {
	if( strcasecmp($promo_code, $coup['code']) == 0) {
		$coupon = $coup;
		break;
	}
}

if(count($coupon) == 0) {
	showError('Invalid Promo Code!!');
} else {
	echo json_encode(array('stat' => 'SUCCESS', 'content' => array('c' => $coupon['code'], 'p' => $coupon['percentage'])));
}

exit(0);
