<?php
/*if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}*/

error_reporting(E_ALL);

$config = array();

$config['toll_free'] = '(844)477-7027';
$config['company_email'] = 'care@brightsuppliesus.com';
$config['site_url'] = 'brightsuppliesus.com';
$config['company_name'] = 'Isidor Bright Makings LLC';
$config['company_address'] = '1360 University Ave W #172, St. Paul, MN 55104, USA'; 
$config['return_address'] = '1360 University Ave W #172, St. Paul, MN 55104, USA';
$cshour ='Monday to Friday between the hours of 9am-6pm EST';

$config['product_name'] = 'Bright Supplies US';
$card_statement = 'brightsuppliesus.com';

$config['max_qty'] = 1;
$config['cart_page'] = "cart.php";

$config['productSpecs'] = array(
	
	//***************************PRODUCT 1********************
								
	//***************************PRODUCT 1********************
								
	1 => array(
		'id' => 1001, 
		'nm' => "Microfibre Towel Set", 
		'prc' => 7.96,
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "This microfiber bath towel can absorb water several times to its own weight which is better than cotton towel.",
		'desc_two' => "",
		'img' => "images/pro-1.png",
		'cartimg' => "<img src='images/pro-1.png'/>",
		'benefits' => "1",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>.  By placing an order with us you will be charged $7.96 + $0.00 for S&H one time for a Microfibre Towel Set. If you are not completely satisfied with Microfibre Towel Set at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "regular",
		'product-type' => "regular",
		'shp-prc' => 0.00,
		
		), 
    
	//*********************PRODUCT 2************************

	2 => array(
		'id' => 1002,  
		'nm' => "EarPod Case", 
		'prc' => 9.99,
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "Save your earpods from impacts by storing it securely in this anti-slip embossed, shock-absorbent protective case with well-defined power button and USB socket that allows switching on and charging while your earpods nest in the case.",
		'desc_two' => "",
		'img' => "images/pro-2.png",
		'cartimg' => "<img src='images/pro-2.png'/>",
		'benefits' => "2",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>.  By placing an order with us you will be charged $9.99 + $0.00 for S&H one time for a EarPod Case. If you are not completely satisfied with EarPod Case at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "Evlau",
		'product-type' => "regular",
		'shp-prc' => 0.00,
		),
	
	
	//*******************PRODUCT 3************************
	3 => array(
		'id' => 1003,  
		'nm' => "Resistance Bands", 
		'prc' => 17.95, 
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "The most comfortable bands with thick, wide, and flexible fabric that stays elastic over time and isn't too tight or loose, giving you the best workout.",
		'desc_two' => "",
		'img' => "images/pro-3.png",
		'cartimg' => "<img src='images/pro-3.png'/>",
		'benefits' => "3",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>.  By placing an order with us you will be charged $17.95 + $0.00 for S&H one time for a Resistance Bands. If you are not completely satisfied with Resistance Bands at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "Evlau",
		'product-type' => "regular",
		'shp-prc' => 0.00,
		
	),

	//******************************PRODUCT 4*********************8
	4 => array(
		'id' => 1004,  
		'nm' => "Gym Ball", 
		'prc' => 7.85,
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "Activate your whole body muscles by using a yoga ball! By controlling the flexible fitness ball for training, you can stretch your body, improve your balance, and relieve the soreness caused by sedentary.",
		'desc_two' => "",
		'img' => "images/pro-4.png",
		'cartimg' => "<img src='images/pro-4.png'/>",
		'benefits' => "4",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>.  By placing an order with us you will be charged $7.85 + $0.00 for S&H one time for a Gym Ball. If you are not completely satisfied with Gym Ball at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "regular",
		'product-type' => "regular",
		'shp-prc' => 0.00,
	),
	
	//******************************PRODUCT 5*********************
	
	5 => array(
		'id' => 1005,  
		'nm' => "Exercise Set", 
		'prc' => 48.79, 
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "This is 5 in 1 home training equipment that includes adjustable jump rope, hand grabber, push bar up, AB roller with knee pad.",
		'desc_two' => "",
		'img' => "images/pro-5.png",
		'cartimg' => "<img src='images/pro-5.png'/>",
		'benefits' => "5",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>.  By placing an order with us you will be charged $48.79 + $0.00 for S&H one time for a Exercise Set. If you are not completely satisfied with Exercise Set at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "regular",
		'product-type' => "regular",
		'shp-prc' => 0.00,
	),
	
	//******************************PRODUCT 6*********************8
	
	6 => array(
		'id' => 1006,  
		'nm' => "Fitness Stepper", 
		'prc' => 49.85,
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "Complete set with two sets of risers therefore three height levels for maximum versatility in your workouts.",
		'desc_two' => "",
		'img' => "images/pro-6.png",
		'cartimg' => "<img src='images/pro-6.png'/>",
		'benefits' => "6",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>.  By placing an order with us you will be charged $49.85 + $0.00 for S&H one time for a Fitness Stepper. If you are not completely satisfied with Fitness Stepper at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "regular",
		'product-type' => "regular",
		'shp-prc' => 0.00,
	),
	
	//******************************PRODUCT 7*********************8
	
	7 => array(
		'id' => 1007,  
		'nm' => "Wireless EarPods", 
		'prc' =>0.00,
		'altnm' => "Try Before You Buy", 
		'desc-1' => "",
		'desc' => "The ambient aware splash resistant Bluetooth earbuds with adaptive dual microphone, substantial bass and simple controls boasts high fidelity output and is compatible with both Android and iOS compatible devices. <span>The descriptor on your credit card bill will be listed as $card_statement.</span>",
		'desc_two' => "",
		'img' => "images/pro-7.png",
		'cartimg' => "<img src='images/pro-7.png'/>",
		'benefits' => "6",
		'terms' => "",
		'chkterms' =>"Wireless EarPods Try Before You Buy Terms: To help you get started, we offer our customers not only products at retail price but also the option for Try Before You Buy. We want you to try the product to determine if it is right for you. By placing an order, you agree to the full <a href='terms-trial.php' target='_blank' >Terms & Conditions</a> and <a href='privacy.php' target='_blank' >Privacy Policy</a> as well as enrollment into our monthly auto-ship program where you will immediately be billed the shipping and handling amount of $9.99 and we will immediately ship your  Wireless EarPods. You have a 10 day trial. Your trial will begin upon receipt of your  Wireless EarPods. After your  Wireless EarPods trial has ended, your credit card will then be automatically charged the full retail price of $97.57 and you will be shipped a recurring supply every 30 days unless you take action to cancel your trial. For all MasterCard transactions ONLY, within 5 days, you will receive an email requiring your response to activate the monthly auto-ship program. If you are happy with your  Wireless EarPods, you are required to consent to the monthly auto-ship program in order to receive additional  Wireless EarPods. You will be charged $97.57 (Free Shipping) for each recurring product that has shipped to you until you cancel. If our product is not right for you, simply call <tt class='trial-trm'>{$config['toll_free']}</tt> or contact us via email at  {$config['company_email']} to cancel your auto-ship membership. Your credit card will be billed as {$card_statement} on your statement.",
       
		'prod-option' => "regular",
		'product-type' => "multi",
		'shp-prc' => 9.99,
	),
	
	8 => array(
		'id' => 1008,  
		'nm' => "Wireless EarPods", 
		'prc' => 97.57,
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "",
		'desc_two' => "",
		'img' => "images/pro-7.png",
		'cartimg' => "<img src='images/pro-7.png'/>",
		'benefits' => "6",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>. Explaining that by placing an order, you will be charged the full price of $97.57 for Wireless EarPods. If you are for any reason unsatisfied with our Wireless EarPods, you can send back any used or unused products for a full refund by calling <tt>{$config['toll_free']}</tt>. Shipping and handling costs are free for one-time purchases. You have 30 days to return the product for a full refund, and you will be responsible for applicable postage charges. Your credit card will be billed as {$card_statement} on your statement.",
		'prod-option' => "",
		'product-type' => "multi",
		'shp-prc' => 0.00,
	),
	
	//******************************PRODUCT 8*********************8
	
	9 => array(
		'id' => 1009,  
		'nm' => "Dumbbell Set", 
		'prc' => 99.69,
		'altnm' => "", 
		'desc-1' => "",
		'desc' => "Pump up your routine with these neoprene coated hand dumbbell pairs which will never rust or damage your floors, and with their anti-rolling hex design, your free weights won't roll away making them easy to stack.",
		'desc_two' => "",
		'img' => "images/pro-8.png",
		'cartimg' => "<img src='images/pro-8.png'/>",
		'benefits' => "6",
		'terms' => "",
		'chkterms' => "&nbsp;I am 18 years or older and agree to the <a href='terms.php' target='blank' style='text-decoration:underline;'>Terms and Conditions</a> and <a href='privacy.php' style='text-decoration:underline;' target='blank'>Privacy Policy</a>. By placing an order with us you will be charged $99.69 + $0.00 for S&H one time for a Dumbell Set. If you are not completely satisfied with Dumbell Set at any time, please call <tt>{$config['toll_free']}</tt>, {$cshour}. You will receive your product within 2-4 business days of payment. Your credit card will be billed as {$card_statement}. on your statement.",
		'prod-option' => "regular",
		'product-type' => "regular",
		'shp-prc' => 0.00,
	),
	
);

$config['shippingRates'] = array(
	'US' => array('s'=> 70, 
		'h'=> 0.00,
		'r' => 0
	)
);

